package com.example.fin.dao;

import com.example.fin.domain.TransactionCategory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionCategoryDao extends JpaRepository<TransactionCategory, Long> {
    Optional<TransactionCategory> findByIdAndUserId(Long categoryId, Long userId);

    Optional<List<TransactionCategory>> findAllByUserId(Long userId, Sort sort);
}
