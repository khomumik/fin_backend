package com.example.fin.dao;

import com.example.fin.domain.Wallet;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WalletDao extends JpaRepository<Wallet, Long> {
    Optional<List<Wallet>> findAllByUserId(Long userId, Sort sort);
    Optional<Wallet> findByIdAndUserId(Long id, Long userId);
}
