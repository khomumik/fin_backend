package com.example.fin.controller;

import com.example.fin.mapper.DtoMapper;
import com.example.fin.mapper.dto.TransactionFullDto;
import com.example.fin.mapper.dto.TransactionPatchRequestDto;
import com.example.fin.mapper.dto.TransactionPostRequestDto;
import com.example.fin.service.TransactionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/transactions")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;
    private final DtoMapper dtoMapper;

    @GetMapping
    public ResponseEntity<List<TransactionFullDto>> getAllTransactions(@RequestParam(defaultValue = "id,asc") String[] sort) {
        Sort sorting = getSort(sort);
        return ResponseEntity.ok(
                transactionService.getAllTransactions(sorting)
                        .stream()
                        .map(dtoMapper::transactionToTransactionFullDto)
                        .toList()
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionFullDto> getOneTransaction(@PathVariable Long id) {
        return ResponseEntity.ok(dtoMapper.transactionToTransactionFullDto(
                transactionService.getOneTransaction(id))
        );
    }

    @PostMapping
    public ResponseEntity<TransactionFullDto> createWallet(@Valid @RequestBody TransactionPostRequestDto request) {
        return ResponseEntity.ok(
                dtoMapper.transactionToTransactionFullDto(
                        transactionService.createTransaction(
                                request
                        )
                )
        );
    }

    @PatchMapping
    public ResponseEntity<TransactionFullDto> updateTransaction(@Valid @RequestBody TransactionPatchRequestDto request) {
        return ResponseEntity.ok(
                dtoMapper.transactionToTransactionFullDto(
                        transactionService.updateTransaction(request)
                )
        );
    }

    @DeleteMapping("/{id}")
    public void deleteTransaction(@Valid @PathVariable Long id) {
        transactionService.deleteTransaction(id);
    }

    protected static Sort getSort(String[] sort) {
        if (!((sort[0].equals("id")
                || sort[0].equals("note")
                || sort[0].equals("amount"))
                &&
                (sort[1].equals("desc")
                || sort[1].equals("asc")))
        ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not correct sorting parameters");
        }
        Sort sorting;
        if (sort[1].equals("desc")) {
            sorting = Sort.by(sort[0]).descending();
        }else {
            sorting = Sort.by(sort[0]).ascending();
        }
        return sorting;
    }
}
